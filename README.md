# Budget Web

Administrative app for Budget.

## Styleguide
Refer to AngularJS John Papa Styleguide: https://github.com/johnpapa/angular-styleguide

## Set Up
NPM and bower have to be installed in order to set up the project.

Run bower and npm to get all dependencies needed.
```sh
  npm install
  bower install
``` 

## Running the app
Run `npm start` for preview.

## Adding new library
When adding new library with either npm or bower use the `--save` or `--save-dev` option.
Also add the new file's path to the gulp task `build-index` in the sources array specifically.
Then run the following command to inject those files into the index `gulp build-index`

## To add the files to the index
When adding a new component to the project, use `gulp build-index` to add the files into the `index.html` file. 

## Testing
Running `karma start` will run the unit tests with karma.

## Deploying to heroku

### Login with your account and select the Heroku project

```shell
$ heroku login
$ heroku git:remote -a budget-nic-staging
```

### Create the `dist` folder

```shell
$ gulp clean
$ gulp build-production
```

### Add the files to your repo and commit them

```shell
$ git add .
$ git commit -m "message" 
```

### Push to Heroku

```shell
$ git push -f heroku <current_branch>:master
```