(function() {
  'use strict';

  angular
    .module('app')
    .run(run);

  run.$inject = ['$window'];

  /* @ngInject */
  function run($window) {

    //staging
    var stagingConfig = {
      apiKey: "AIzaSyD5RHeoUGIylWvnUnu4i6rFHA-Y_JHzL2g",
      authDomain: "budget-nic-developer.firebaseapp.com",
      databaseURL: "https://budget-nic-developer.firebaseio.com",
      projectId: "budget-nic-developer",
      storageBucket: "budget-nic-developer.appspot.com",
      messagingSenderId: "860073900558"
    };

    //prod
    var productionConfig = {
      apiKey: "AIzaSyA1xp7UhFGmVRR7yHi36vR2ncGSR0Qrlno",
      authDomain: "budget-nic.firebaseapp.com",
      databaseURL: "https://budget-nic.firebaseio.com",
      storageBucket: "budget-nic.appspot.com",
      messagingSenderId: "128066998170"
    };

    $window.firebase.initializeApp(stagingConfig);

  }
})();
