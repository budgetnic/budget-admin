(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController(authService, sessionService, $state) {

    var vm = this;

    vm.closeAlert = closeAlert;
    vm.logIn = logIn;

    function closeAlert() {
      vm.isIncorrectLogin = false;
    }

    function logIn(username, password) {
      authService.logIn(username, password)
        .then(redirectToHome)
        .catch(showError);
    }

    function showError(error) {
      vm.isIncorrectLogin = true;
    }

    function redirectToHome(user) {
      if (user) {
        sessionService.setAuthData(user.email);
        $state.go('revisions');
      }
    }

  }
})();

