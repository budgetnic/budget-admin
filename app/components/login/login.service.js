(function() {
  'use strict';

  angular
    .module('app.login')
    .factory('authService', authService);

  /* @ngInject */
  function authService($state, $rootScope, $location, sessionService) {

    var service = {
      isLoggedIn: isLoggedIn,
      logIn: logIn,
      logOut: logOut,
      verifyAccess: verifyAccess
    };

    return service;

    function isLoggedIn() {
      var authData = sessionService.getAuthData();
      var sessionDefined = typeof authData !== 'undefined';
      var authDataDefined = authData !== null;

      return sessionDefined && authDataDefined;
    }

    function logIn(email, password) {
      var emailUser = email + '@budget.com';
      return firebase.auth().signInWithEmailAndPassword(emailUser, password);
    }

    function logOut() {
      sessionService.destroy();
      $state.go('login');
    }

    function verifyAccess() {
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        if(!service.isLoggedIn() && toState.name !== 'login') {
          event.preventDefault();
          service.redirectInfo = {};
          service.redirectInfo.toStateName = toState.name;
          service.redirectInfo.toParams = toParams;
          logOut();
          redirect();
        }

      });
    }

    function redirect() {
      if(service.redirectInfo && isLoggedIn()) {
        $state.go(service.redirectInfo.toStateName, service.redirectInfo.toParams);
      } else if(!service.redirectInfo && isLoggedIn()) {
        $state.go('revisions');
      } else {
        $state.go('login');
      }
    }

  }

})();

