(function() {
  'use strict';

  angular
    .module('app.revisions')
    .constant('TIRE_BRANDS', [
      {name: 'Dunlop'},
      {name: 'Bridgestone'},
      {name: 'Yokohama'},
      {name: 'Firestone'},
      {name: 'Pirelli'},
      {name: 'Kumho'},
      {name: 'Hankook'},
      {name: 'Goodyear'},
      {name: 'Michelin'},
      {name: 'Toyo'},
      {name: 'Otros'}
    ])
    .constant('REVISION_TYPES', [
      {name: 'check-in'},
      {name: 'check-out'}
    ])
    .constant('CAR_PARTS', [
      {'name': 'Antena' ,  'key': 'antenna'},
      {'name': 'Llanta de repuesto' , 'key': 'spare_tire'},
      {'name': 'Radio' , 'key': 'radio'},
      {'name': 'Cámara' , 'key': 'camera'},
      {'name': 'Herramientas' , 'key': 'tools'},
      {'name': 'Triángulo' , 'key': 'triangle'},
      {'name': 'Extinguidor' , 'key': 'extinguisher'},
      {'name': 'Rodamiento del año' , 'key': 'bearing_year'},
      {'name': 'Circulación' , 'key': 'circulation'},
      {'name': 'Seguro' , 'key': 'insurance'},
      {'name': 'Emisión de Gases' , 'key': 'emission_gases'},
      {'name': 'Inspección Mecanica' , 'key': 'mechanical_inspection'},
      {'name': 'Rack', 'key': 'rack'},
      {'name': 'Alfombras' , 'key': 'carpet'},
      {'name': 'Emblemas', 'key': 'emblems'},
      {'name': 'Placas', 'key': 'plates'},
      {'name': 'Loderas' , 'key': 'mud_flap'},
      {'name': 'Llave' , 'key': 'keey'},
      {'name': 'Forros' , 'key': 'seat_covers'},
      {'name': 'Pide Vías' , 'key': 'side_lights'},
      {'name': 'Cañuelas' , 'key': 'roof_molding'},
      {'name': 'Cepillos Tricos' , 'key': 'windscreen'},
      {'name': 'Llavines' , 'key': 'doorlock'},
      {'name': 'Retrovisor Interno' , 'key': 'rear_view_mirror'},
      {'name': 'Tapón de Combustible' , 'key': 'fuel_cap'},
      {'name': 'Tapones de Motor' , 'key': 'engine_caps'}
    ])
    .constant('GAS_STATES', [
      {name: 'Vacio'},
      {name: '1/8'},
      {name: '1/4'},
      {name: '3/8'},
      {name: '1/2'},
      {name: '5/8'},
      {name: '7/8'},
      {name: 'Lleno'}
    ])
    .constant('VEHICLES', [
      {id: 'MICROSUV', url: 'assets/img/microSUV.svg'},
      {id: 'TRUCK', url: 'assets/img/truck.svg'},
      {id: 'JEEP', url: 'assets/img/jeep.svg'},
      {id: 'SUV', url: 'assets/img/suv.svg'},
      {id: 'SEDAN_HATCHBACK', url: 'assets/img/sedanHatchback.svg'},
      {id: 'SEDAN', url: 'assets/img/sedan.svg'}
    ]);

})();
