(function() {
  'use strict';

  angular
    .module('app.revisions')
    .factory('revisionsEmailService', revisionsEmailService);

  /* @ngInject */
  function revisionsEmailService(REVISIONS_URL, $http, $filter) {

    var service = {
      url: REVISIONS_URL,
      sendEmail: sendEmail,
      formatRevisionToJson: formatRevisionToJson,
      getNewObservations: getNewObservations,
      formatDamagesToJson: formatDamagesToJson,
      getComment: getComment
    };

    return service;

    function sendEmail(revision, comment, email, ccMail, language) {
      return $http.post(service.url, service.formatRevisionToJson(revision, comment, email, ccMail, language), { "Content-Type": "application/json" });
    }

    function formatRevisionToJson(revision, comment, email, ccMail, language) {
      var dateFilter = $filter('date');
      var damages = revision.damages;
      var observations = revision.observations;
      var json = {
        damages: service.formatDamagesToJson(damages),
        observations: service.getNewObservations(observations),
        vehicleType: revision.vehicleType,
        revision: {
          gasLevel: revision.gas_level ? revision.gas_level:'',
          deliveryPlace: revision.delivery_place.name ? revision.delivery_place.name:'',
          km: revision.km ? revision.km:'',
          timestamp: dateFilter(revision.timestamp, 'dd-MM-yyyy HH:mm:ss'),
          type: revision.type ? revision.type:'',
          username: revision.username ? revision.username:'',
          vehicleMVA: revision.vehicle_ref,
          carParts: revision.car_parts_present,
          canvas: revision.canvas,
          contractNumber: revision.contract_number ? revision.contract_number:'',
          drive: revision.drive ? revision.drive:'',
          phone: revision.phone ? revision.phone:'',
          optionType: revision.optionType ? revision.optionType:'',
          delivery_place_d: revision.delivery_place_d ? revision.delivery_place_d:'',
          delivery_place_o: revision.delivery_place_o ? revision.delivery_place_o:'',
          numberMovement: revision.numberMovement ? revision.numberMovement :'',
          checkOutTime: dateFilter(revision.checkOutTime, 'dd-MM-yyyy HH:mm:ss'),
          date: dateFilter(revision.timestamp, 'dd-MM-yyyy HH:mm:ss'),
          isTimeExceeded: ((revision.optionType === 'CarMov' || revision.optionType === 'Other') && revision.deliveryTimeInfo) ? revision.deliveryTimeInfo.isTimeExceeded : false,
          deliveryTime: ((revision.optionType === 'CarMov' || revision.optionType === 'Other') && revision.deliveryTimeInfo) ? dateFilter(revision.deliveryTimeInfo.deliveryTime, 'HH:mm:ss') : ''
        },
        deliveryPlaceMail: revision.delivery_place.email,
        ccMail: ccMail,
        email: email,
        language: language,
        comment: service.getComment(comment, language),
        signature: '',
      };
      return json;
    }

    function formatDamagesToJson(damages) {
      var formmattedDamages = [];
      damages.forEach(function(damage) {
        formmattedDamages.push({ damage: damage.damage_type, part: damage.part, isNew: damage.is_new, severity: damage.severity_type});
      });
      return formmattedDamages;
    }

    function getNewObservations(observations) {
      var newObservations = [];
      if(observations) {
        observations.forEach(function(observation) {
          newObservations.push(observation.observation);
        });
      }
      if(newObservations.length === 0) newObservations.push('Sin observaciones');
      return newObservations;
    }

    function getComment(comment, language) {
        var text = (language === "spanish") ? "Sin comentarios" : "No comments";
        return (comment === "") ? text : comment;
    }
  }

})();
