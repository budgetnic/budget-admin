(function() {

  angular
    .module('app.revisions')
    .controller('PrintPDFModalController', PrintPDFModalController);

  /* @ngInject */
  function PrintPDFModalController($uibModalInstance, revisionsPDFService, revision, comment, addAlert) {

    var vm = this;
    vm.revision = revision;
    vm.comment = comment;
    vm.addAlert = addAlert;
    vm.sendRevision = sendRevision;
    vm.cancel = cancel;

    activate();

    function activate(){
      vm.language = 'spanish';
    }

    function sendRevision(revision, comment, language) {
      $uibModalInstance.close(revisionsPDFService.getPDF(revision, comment, language));
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();
