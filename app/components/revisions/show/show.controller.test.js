(function() {
  'use strict';

  describe('ShowRevisionController', function() {

    var $controller, $q, $state, stateParams, deferredLoadRevision, showService, $uibModal, $rootScope, $scope, loadRevisionDataDeferred, CAR_PARTS;
    var REVISION_ID = 2435624;

    beforeEach(module('app.core'));
    beforeEach(module('app.revisions'));
    beforeEach(module(function($provide) {
      $provide.service('showService', function() {
        this.loadRevisionData = loadRevisionData;
        this.resetRevisionData = resetRevisionData;
        this.getRevision = getRevision;
        this.getDamages = getDamages;
        this.getNewObservations = getNewObservations;
        this.CAR_PARTS = [];
      });

      var loadRevisionData = function(id) {
        loadRevisionDataDeferred = $q.defer();
        return loadRevisionDataDeferred.promise;
      };
      var getRevision = function() { return {}};
      var getDamages = function() { return {}};
      var resetRevisionData = function() {};
      var getNewObservations = function() {return {}}
    }));

    beforeEach(module(function($provide) {
      $provide.service('authService', function() {
        this.logOut = function() {};
        this.isLoggedIn = function() {};
        this.verifyAccess = function() {};
      });
    }));

    beforeEach(inject(
      function(_$controller_, _$q_, _$state_, _showService_, _$rootScope_, _$uibModal_) {
        $q = _$q_;
        $state = _$state_;
        $rootScope = _$rootScope_;
        showService = _showService_;
        $scope = $rootScope.$new();
        $uibModal = _$uibModal_;
        deferredLoadRevision = $q.defer();
        stateParams = {id: REVISION_ID};
        spyOn($state, 'go').and.callFake(function(id) {});

        $controller = _$controller_('ShowRevisionController', {
          '$state': $state,
          '$stateParams': stateParams,
          '$uibModal': $uibModal,
          '$rootScope': $rootScope,
          '$scope': $scope,
          'showService': showService,
          'CAR_PARTS': CAR_PARTS
        });
      }
    ));

    it ('should be defined', function () {
      expect($controller).toBeDefined();
    });

    it ('should call state go', function(){
      $controller.editRevision();
      expect($state.go).toHaveBeenCalled();
    });


  });

})();
