(function() {
  'use strict';

  angular
    .module('app.revisions')
    .factory('showService', showService);

  /* @ngInject */
  function showService(vehiclesService, deliveryPlacesService, revisionsService, $q) {

    var service = {
      vehicle: '',
      revision: {},
      loadRevisionData: loadRevisionData,
      setRevision: setRevision,
      setObservations: setObservations,
      setDamages: setDamages,
      setDeliveryPlaces: setDeliveryPlaces,
      getRevision: getRevision,
      setVehicle: setVehicle,
      getVehicle: getVehicle,
      getDamages: getDamages,
      setCanvas: setCanvas,
      getNewObservations: getNewObservations,
      resetRevisionData: resetRevisionData
    };

    return service;

    function loadRevisionData(vehicleId){
      var deferred = $q.defer();
      revisionsService.getRevision(vehicleId)
        .then(function(revision) {
          service.setRevision(revision);
          loadDamagesAndObservations()
            .then(function(){
              vehiclesService.findById(service.getRevision().vehicle_ref)
              .then(function(vehicle) {
                service.setVehicle(vehicle);
                deferred.resolve();
              });
            deliveryPlacesService.get(service.getRevision().delivery_place_ref)
              .then(function(deliveryPlace){
                service.setDeliveryPlaces(deliveryPlace);
              });
          });
        });
      return deferred.promise;
    }

    function loadDamagesAndObservations() {
      return $q.all([loadDamages(), loadObservations()]);
    }

    function setRevision(revision){
      service.revision = revision;
    }

    function setDamages(damages){
      service.revision.damages = damages;
    }

    function setObservations(observations){
      service.revision.observations = observations;
    }

    function setDeliveryPlaces(deliveryPlace){
      service.revision.delivery_place = deliveryPlace;
    }

    function setVehicle(vehicle){
      service.vehicle = vehicle;
    }

    function getVehicle(){
      return service.vehicle;
    }

    function getRevision(){
      return service.revision;
    }

    function getDamages(){
      return service.revision.damages;
    }

    function getNewObservations(){
      return service.observations;
    }

    function setCanvas(canvas){
     service.revision.canvas = canvas;
    }

    function resetRevisionData(){
      service.vehicle = '';
      service.revision = {};
    }

    function loadDamages(){
      var damagesRef = service.getRevision().damages_ref;

      if (damagesRef){
        return revisionsService.getDamages(damagesRef)
          .then(function(damages) {
            setDamages(damages);
          });
      }
    }

    function loadObservations(){
      var observationsRef = service.getRevision().observations_ref;

      if(observationsRef){
        return revisionsService.getObservations(observationsRef)
          .then(function(observations) {
            setObservations(observations);
          });
      }
    }
  }
})();
