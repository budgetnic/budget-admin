(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('ShowUserController', ShowUserController);

  /* @ngInject */
  function ShowUserController($stateParams, usersService, lodash, $q) {

    var vm = this;
    vm.fetchUser = fetchUser;

    fetchUser($stateParams.id);

    function fetchUser(userId) {
      usersService.findById(userId)
        .then(function(user) {
          vm.user = user;
          vm.delivery_place_ref = lodash.find(usersService.getDeliveryPlaces(),
           {"$id" : vm.user.delivery_place_ref});
        });
    }

  }

})();
