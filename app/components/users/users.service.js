(function() {
  'use strict';

  angular
    .module('app.users')
    .factory('usersService', usersService);

  /* @ngInject */
  function usersService($firebaseObject) {

    var rootRef = firebase.database().ref();

    var service = {
      deliveryPlaces: {},
      getAll: getAll,
      findById: findById,
      deleteUser: deleteUser,
      create: create,
      getDeliveryPlaces: getDeliveryPlaces,
      setDeliveryPlaces: setDeliveryPlaces
    };

    return service;

    function getAll() {
      var usersRef = rootRef.child('users');
      return $firebaseObject(usersRef).$loaded();
    }

    function findById(id) {
      var userRef = rootRef.child('users').child(id);
      return $firebaseObject(userRef).$loaded();
    }

    function create(user) {
      var newPath = rootRef.child('users').child(user.username);
      return newPath.set(user);
    }

    function deleteUser(user) {
      return user.$remove();
    }

    function setDeliveryPlaces(deliveryPlaces) {
      service.deliveryPlaces = deliveryPlaces;
    }

    function getDeliveryPlaces() {
      return service.deliveryPlaces;
    }

  }

})();
