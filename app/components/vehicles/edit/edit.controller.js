(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .controller('EditVehicleController', EditVehicleController);

  /* @ngInject */
  function EditVehicleController($state, deletionModalService, $stateParams, vehiclesService, VEHICLES_TYPES) {
    var vm = this;
    vm.persistData = persistData;
    vm.openDeleteModal = openDeleteModal;
    vm.vehiclesTypes = VEHICLES_TYPES;

    vehiclesService.findById($stateParams.id)
      .then(function(vehicle) {
        vm.vehicle = vehicle;
      });

    function persistData(vehicle) {
      vehicle.$save()
        .then(function() {
          $state.go('vehicle', {id: vehicle.$id});
        });
    }

    function openDeleteModal() {
      var modalInstance = deletionModalService.open("vehicle");

      modalInstance.result
        .then(function(success) {
          vehiclesService.deleteVehicle(vm.vehicle).then(function() {
            $state.go('vehicles');
          });
        });
    }

  }

})();

