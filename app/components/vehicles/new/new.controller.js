(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .controller('NewVehicleController', NewVehicleController);

  /* @ngInject */
  function NewVehicleController($state, vehiclesService, VEHICLES_TYPES) {
    var vm = this;
    vm.vehicle = {};
    vm.vehicle.gas = 'Gasolina';
    vm.vehicle.traction_type = '4x2';
    vm.vehiclesService = vehiclesService;
    vm.persistData = persistData;
    vm.vehiclesTypes = VEHICLES_TYPES;

    activate();

    function activate() {
      defaultVehicleDefaultType();
    }

    function persistData(vehicle) {
      vehicle = formatVehicleObject(vehicle);
      vm.vehiclesService.create(vehicle)
        .then(function() {
          $state.go('vehicle', {id: vehicle.MVA});
        });
    }

    function defaultVehicleDefaultType() {
      vm.vehicle.type = VEHICLES_TYPES[0];
    }

    function formatVehicleObject(vehicle) {
      var typeId = vehicle.type.id;
      vehicle.type = typeId;
      return vehicle;
    }

  }

})();

