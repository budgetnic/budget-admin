(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .controller('ShowVehicleController', ShowVehicleController);

  /* @ngInject */
  function ShowVehicleController($stateParams, vehiclesService, VEHICLES_TYPES, lodash) {
    var vm = this;

    activate();

    function activate() {
      vehiclesService.findById($stateParams.id)
      .then(
        function(vehicle) {
          vm.vehicle = vehicle;
          defineType(vehicle.type);
        });
      }

    function defineType(vehicleType) {
      var vehicle = _.find(VEHICLES_TYPES, {
        id: vehicleType
      });

      vm.vehicle.type = vehicle.name;
    }
  }

})();
