(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .constant('VEHICLES_TYPES', [
      {id: 'MICROSUV', name: 'Microbús SUV'},
      {id: 'TRUCK', name: 'Camioneta Doble Cabina'},
      {id: 'JEEP', name: 'Jeep de 2 Puertas'},
      {id: 'SUV', name: 'SUV'},
      {id: 'SEDAN_HATCHBACK', name: 'Sedan Hatchback'},
      {id: 'SEDAN', name: 'Sedan'}
    ]);
})();