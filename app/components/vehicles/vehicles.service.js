(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .factory('vehiclesService', vehiclesService);

  /* @ngInject */
  function vehiclesService($firebaseObject) {

    var rootRef = firebase.database().ref();

    var service = {
      getAll: getAll,
      findById: findById,
      deleteVehicle: deleteVehicle,
      create: create
    };

    return service;

    function getAll() {
      var vehicles = rootRef.child('vehicles');
      return $firebaseObject(vehicles).$loaded();
    }

    function findById(id) {
      var vehicle = rootRef.child('vehicles').child(id);
      return $firebaseObject(vehicle).$loaded();
    }

    function create(vehicle) {
      var newPath = rootRef.child('vehicles').child(vehicle.MVA);
      return newPath.set(vehicle);
    }

    function deleteVehicle(vehicle) {
      return vehicle.$remove();
    }

  }

})();

