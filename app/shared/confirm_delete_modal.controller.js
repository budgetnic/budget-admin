(function() {

  angular
    .module('app.core')
    .controller('ConfirmDeletionModalController', ConfirmDeletionModalController);

  /* @ngInject */
  function ConfirmDeletionModalController($uibModalInstance, objectType) {

    var vm = this;
    vm.cancel = cancel;
    vm.confirm = confirm;

    activate();

    function activate() {
      if (objectType == "vehicle") {
        vm.message = 'Está seguro que desea eliminar el vehículo?';
      }
      if (objectType == "user") {
        vm.message = 'Está seguro que desea eliminar el usuario?';
      }
      if (objectType == "revision") {
        vm.message = 'Está seguro que desea eliminar la revisión?';
      }
    }

    function confirm() {
      $uibModalInstance.close();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();

